#ifndef ACTIVUTIL_H
#define ACTIVUTIL_H
#include "leafnode.h"

int compactive(const void *, const void *);
void validateactive(void);
void newsgroup_copy(struct newsgroup *d, const struct newsgroup *s);

#endif
