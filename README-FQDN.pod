=head1 The unique fully-qualified domain name

This document will introduce you to the Message-ID and FQDN difficulties
and help you out.

=head1 What is this fully-qualified domain name or FQDN?

The unique fully-qualified domain name (or host name, FQDN or FQHN for
short) is used by leafnode to generate the Message-ID.

What is the Message-ID?

Every message (mail or news) has a unique piece of text in its header,
the so-called Message-ID. It is not usually visible in your software unless
you choose "display all headers" or "display raw" or "display source". 

The Message-ID looks roughly like a strange E-Mail address,
E<lt>12345.67890@host.example.comE<gt>: it has two parts. Left of the @
is the "local" part, right of the "@" is the domain part. The local part
of the Message-ID is different for different pieces of mail or news,
while the domain part of the mail address is constant. The domain part
is usually the name of the host that generated the Message-ID: your
computer's name when you are sending mail or posting news.

Whenever a news server is offered a new news article for redistribution,
it looks at the Message-ID to determine if it already has the article,
to avoid double work, and to avoid that new articles run in circles.

Therefore, each message sent, mail or news, must have a B<unique>
Message-ID. If the Message-ID is not unique, because you use the same
host name as somebody else, and you and the other person write an
article at the same time, the article that arrives later will be
considered a I<duplicate> or I<Message-ID collision> and be discarded by
the server - and as messages take a few seconds from one end of the
world to another, two servers at different ends of the net may see the
messages arrive in reverse order.

Leafnode will tell you that the Message-ID of the article it is about to
post is already in use upstream.

Please note that B<this problem is not leafnode-specific>. Any mail or
news software may suffer from collisions, but it's less prominent in
mail, and it's difficult to see at all, because the messages disappear
silently, no message will be coming back to tell you another message was lost.

=head1 How to get a unique fully-qualified domain name

There are several ways to obtain one. When you got yours, see L<below on
how to configure your domain name|/"How to configure the fully-qualified
domain name">.

=over

=item If you have a domain registered:

Assume you are the rightful owner of example.com. You can now reserve
any sub domain you wish, say mid.example.com, and a host name for your
leafnode computer, say, abacus.mid.example.com, or for a friend. How you
track that only one machine has the same name at the same time, is up to
you. Writing I<gave abacus.mid.example.com to Joe at 2002-07-11> on a
sheet of paper is sufficient - if you can find this sheet later on.

Again: This host name B<need not> be entered into your DNS data base,
just make sure only one computer uses this name at the same time.

=item If you have an account at C<news.individual.de> or
C<news.individual.net>:

You have been assigned a user ID. To find it out, type in your shell:

 telnet news.individual.de 119

(wait until connected)

 authinfo user sixpack
 authinfo pass joe
 quit

Replace "sixpack" and "joe" by your login and password. After the
"authinfo pass" line, you should see a line that reads:

 281 Authentication accepted. (UID=00000)

If you get a 481 line, please retry, you may have mistyped user name or
password. Correcting these lines with Backspace or Delete may also lead
to failed logins. Retry with careful typing so that you do not need to
correct your input.

The server would have printed your user ID where my example shows 00000.

Your hostname then is ID-00000.user.uni-berlin.de. DO MAKE SURE TO
REPLACE THE NUMBER IN ID-00000 by the number the server told you in the
UID= LINE.

=item Specific providers:

=over

=item T-Online

T-Online customers, your hostname is NNNNN.dialin.t-online.de, where
NNNNN is your T-Online number. If your T-Online number contains your
telephone number, contact T-Online support to have a new T-Online number
assigned. I'm unaware if they charge you for this change.

=back

=item Ask your network administrator or your Internet service provider.

Your local network administrator can assign you a domain to use for
Message-IDs.

Your Internet service provider may have reserved a special sub domain for
the sole purpose of letting users create their own unique Message-IDs.

=back

=head2 When leafnode ignores your host name

Well, it is probably the default name or domain that your OS vendor
chose, like "localhost.localdomain". As such, it is not unique, but used
on many computers, and can therefore cause collisions and in severe
cases make your articles disappear.

=head2 Why localhost.ANYTHING will not work

Many sites run resolvers that are based on ISC Bind code. And many sites
configure their name servers so that they will resolve localhost.example.com.
Therefore, localhost.example.com will not designate a single computer,
but any computer that has "localhost" as a name. These resolvers are
problematic because they will first see the domain as unqualified and
append the domain or searchlist, so assuming that your domain is
example.com, these resolvers will try localhost.example.com first, which
will resolve to 127.0.0.1 at many sites.

(It is usually a mistake to add localhost to the name server for a
domain, the clients had better be fixed instead. As a workaround,
removing all domain and searchlist lines from C</etc/resolv.conf> will
usually work at the expense of not being able to use short names unless
they are listed in C</etc/hosts>.)

=head1 How to configure the fully-qualified domain name

=head2 System-wide

Preferably, the host name is entered into your system configuration so
that it is available globally, to your mailers and news readers should
they desire to create a FQDN.

How exactly the hostname is configured, depends on your system, it is
usually a two-step approach, but your system installation/configuration
software may simplify things for you.

1. Configure the bare hostname. Every system has its own favorite
configurator, but you'll find this in the index. SuSE Linux 8.0
and newer read the hostname from the file C</etc/HOSTNAME>. FreeBSD 4
and newer reads it from the C<hostname> variable in C</etc/rc.conf>.

2. On many systems, you will have to put the fully qualified host name
into C</etc/hosts>, too, so the system can find out the full name with
domain if given just its bare name without a domain. (On networked
systems, using NIS, DNS or LDAP is also feasible if the client is
configured to use the respective system to resolve host names.) Usually,
a computer that is to resolve a hostname will look at C</etc/hosts>
first and then at DNS.

An C</etc/hosts> line might look like this:

 192.168.0.1 abacus.mid.example.com abacus oldname

Keep the original name of the computer as an alias in case you
configured some other software to use the old name.

=head2 Local to leafnode

You can also write a line like

 hostname = abacus.mid.example.com

into your C</etc/leafnode/config>. But I recommend against doing that,
see the next section why:

=head2 Should I configure the FQDN system-wide or local to leafnode?

You should configure the FQDN system-wide. Your news reader may generate 
a Message-ID itself, and it is not aware of leafnode's configuration and
will generate an invalid Message-ID -- leafnode will then reject the
posting because the Message-ID is invalid.

=head1 Author, Copyright

This document was written and is (C) Copyright 2002, 2003, 2004, 2006
by Matthias Andree.

Permission is hereby granted, free of charge, to any person obtaining a
copy of this software and associated documentation files (the
"Software"), to deal in the Software without restriction, including
without limitation the rights to use, copy, modify, merge, publish,
distribute, sublicense, and/or sell copies of the Software, and to
permit persons to whom the Software is furnished to do so, subject to
the following conditions:

The above copyright notice and this permission notice shall be included
in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS
OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
