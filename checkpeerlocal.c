/* checkpeerlocal.c -- check if the peer address of a socket is on a
 *                     local network.
 * (C) 2002, 2003, 2009, 2010, 2011, 2014, 2015
 * by Matthias Andree <matthias.andree@gmx.de>
 *
 * This library is free software; you can redistribute it and/or modify
 * it under the terms of version 2 of the GNU Lesser General Public License as
 * published by the Free Software Foundation.
 *
 * This library is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library (look for the COPYING.LGPL file); if
 * not, write to the Free Software Foundation, Inc., 59 Temple Place,
 * Suite 330, Boston, MA 02111-1307 USA
 */

#include "leafnode.h"

#include "config.h"
#include "mastring.h"
#include "critmem.h"

#include <sys/types.h>
#include <sys/socket.h>
#ifdef HAVE_SYS_SOCKIO_H
/* needed for SunOS 5, IRIX, ... */
#include <sys/sockio.h>
#endif
#include <sys/ioctl.h>
#include <net/if.h>
#include <netinet/in.h>
#include <errno.h>
#ifndef __LCLINT__
#include <arpa/inet.h>
#endif /* not __LCLINT__ */
#include <unistd.h>
#include <stdlib.h>
#include <string.h>
#include <stddef.h>
#ifdef HAVE_NET_IF_VAR_H
#include <net/if_var.h>
#endif
#ifdef HAVE_NETINET_IN_VAR_H
#include <netinet/in_var.h>
#endif
#include "strlcpy.h"
#include <assert.h>
#ifdef HAVE_GETIFADDRS
#include <ifaddrs.h>
#endif
#ifdef HAVE_IPV6
#include <netdb.h>
#endif

#ifdef TEST
#define D(a) a
#else
#define D(a)
#endif

#ifdef HAVE_GETIFADDRS
#ifdef HAVE_IPV6
/** check if IPv6 address \a a2 is within network specified by address
 * \a a1 and netmask \a n1. If \a n1 is NULL, no netmask is applied.
 * \a a1 and \a a2 must not be NULL. */
static int matchv6addr(const struct sockaddr_in6 *a1,
	const struct sockaddr_in6 *n1,
	const struct sockaddr_in6 *a2)
{
    int i;

    for (i = 0; i != 16 ; i++) {
	if (n1) {
	    if ((a1->sin6_addr.s6_addr[i] & n1->sin6_addr.s6_addr[i])
		    != (a2->sin6_addr.s6_addr[i] & n1->sin6_addr.s6_addr[i]))
		return 0;
	} else {
	    if (a1->sin6_addr.s6_addr[i] != a2->sin6_addr.s6_addr[i])
		return 0;
	}
    }
    return 1;
}
#endif

/** Check if address \a a2 is in network addr \a a1 with netmask \a n1
 * \return 1 if true, 0 otherwise */
static int matchaddr(const struct sockaddr *a1 /** address 1 */,
	const struct sockaddr *n1 /** netmask 1, NULL for "all-ones" mask */,
	const struct sockaddr *a2 /** address 2 */)
{
    assert(a1 != NULL);
    assert(a2 != NULL);

    if (a1->sa_family == a2->sa_family) {
	switch(a1->sa_family) {
	    case AF_INET:
		if (n1)
		    return ((((struct sockaddr_in *)a1)->sin_addr.s_addr & ((struct sockaddr_in *)n1)->sin_addr.s_addr)
			    == (((struct sockaddr_in *)a2)->sin_addr.s_addr & ((struct sockaddr_in *)n1)->sin_addr.s_addr)) ? 1 : 0;
		else
		    return (((struct sockaddr_in *)a1)->sin_addr.s_addr
			    ==((struct sockaddr_in *)a2)->sin_addr.s_addr) ? 1 : 0;
		break;
#ifdef HAVE_IPV6
	    case AF_INET6:
		return matchv6addr((struct sockaddr_in6 *)a1,
			(struct sockaddr_in6 *)n1,
			(struct sockaddr_in6 *)a2);
		break;
#endif
	}
    }

    return 0;
}
#endif

#ifdef TEST
static struct sockaddr_storage tstaddr;

/** Print address type and address given in \a addr. */
static void pat(const struct sockaddr *addr)
{
    char buf[512]; /* RATS: ignore */
    const char *tag = "";
    if (addr == 0) {
	printf("(null)\n");
	return;
    }

    switch (addr->sa_family) {
#ifdef HAVE_IPV6
	case AF_INET6:
	    inet_ntop(addr->sa_family,
		    &((struct sockaddr_in6 *)addr)->sin6_addr, buf, sizeof(buf));
	    tag = "IPv6: ";
	    break;
#endif
	case AF_INET:
	    strlcpy(buf, inet_ntoa(((struct sockaddr_in *)addr)->sin_addr),
		    sizeof(buf));
	    tag = "IPv4: ";
	    break;
#ifdef AF_PACKET
	case AF_PACKET:
	    buf[0] = '\0';
	    tag = "(raw packet)";
	    break;
#endif
	default:
	    snprintf(buf, sizeof(buf), "unsupported address family #%d", addr->sa_family);
	    break;
    }
    printf("%s%s\n", tag, buf);
}
#endif

/** extract IPv4 address from IPv6-mapped IPv4 address (::ffff:10.9.8.7)
 */
#if HAVE_IPV6
static void extract_v6mappedv4(const struct sockaddr_in6 *i6, struct sockaddr_in *out)
{
    out->sin_family = AF_INET;
    out->sin_port = i6->sin6_port;
    memcpy(&out->sin_addr, &(i6->sin6_addr.s6_addr[12]), 4);
}
#endif

/**
 * check whether the peer of the socket is local to any of our network
 * interfaces, \return
 * - -1 for error (check errno),
 * - -2 for other error (not mappable in errno),
 * -  0 for not in local networks
 * -  1 for socket in local networks, or if sock is not a network socket.
 */
#ifdef HAVE_GETIFADDRS
int checkpeerlocal(int sock)
{
    struct ifaddrs *ifap, *ii;
    union {
	struct sockaddr_storage ss;
	struct sockaddr_in sin;
	struct sockaddr_in6 sin6;
	struct sockaddr sa;
    } addr;
    int rc = -2;

#ifdef TEST_LOCAL
    (void)sock;
    memcpy(&addr, &tstaddr, sizeof(addr));
#else
    {
	socklen_t size = sizeof(addr);

	/* obtain peer address */
	if (getpeername(sock, &addr.sa, &size)) {
	    if (errno == ENOTSOCK)
		return 1;
	    else
		return -1;
	}
    }
#endif

#ifdef HAVE_IPV6
    if (IN6_IS_ADDR_V4MAPPED(&addr.sin6.sin6_addr)) {
	    /* map to IPv4 */
	    struct sockaddr_in si;
	    D((printf("IPv4 mapped IPv6 address: ")));
	    extract_v6mappedv4(&addr.sin6, &si);
	    memcpy(&addr.sin, &si, sizeof(struct sockaddr_in));
	    D(pat(&addr.sa));
    }
#endif

    if (getifaddrs(&ifap) != 0) {
	D(printf("getifaddrs failed: %s\n", strerror(errno)));
	return -1;
    }

    for(ii=ifap; ii!=NULL; ii=ii->ifa_next) {
	/* skip interfaces without address */
	if (!ii->ifa_addr) continue;
	/* skip interfaces that are down */
	if ((ii->ifa_flags & IFF_UP) == 0) continue;

	D(printf("interface %s, ", ii->ifa_name));
	D(pat(ii->ifa_addr));
	switch (ii->ifa_addr->sa_family) {
	    case AF_INET:
#ifdef HAVE_IPV6
	    case AF_INET6:
#endif
		if (ii->ifa_flags & IFF_POINTOPOINT) {
		    D(printf("point-to-point ")); D(pat(ii->ifa_dstaddr));
		    /* check local end of p2p interface and accept it */
		    if (matchaddr(ii->ifa_addr, NULL, &addr.sa)) {
			rc = 1;
			goto found;
		    }
		    /* if the p2p interface has no remote address, skip */
		    if (!ii->ifa_dstaddr)
			continue;
		    /* otherwise, p2p interface has a remote address
		     * and also accept the peer */
		    if (matchaddr(ii->ifa_dstaddr, NULL, &addr.sa)) {
			rc = 1;
			goto found;
		    }
		} else {
		    D(printf("netmask "));
		    D(pat(ii->ifa_netmask));
		    if (matchaddr(ii->ifa_addr, ii->ifa_netmask, &addr.sa)) {
			rc = 1;
			goto found;
		    }
		}
		break;
	    default:
		D(printf("skipping\n"));
		continue;
	}
    }
    rc = 0;

found:
    freeifaddrs(ifap);
    return rc;
}
#else
int checkpeerlocal(int sock)
{
    union {
	struct sockaddr_storage ss;
	struct sockaddr_in sin;
	struct sockaddr_in6 sin6;
	struct sockaddr sa;
    } addr;
    mastr *buf;
    struct ifconf ifc;
    struct ifreq *ifr, *end;
    int type;
    socklen_t size;
    int newsock;

    /* obtain peer address */
    size = sizeof(addr);
#ifdef TEST_LOCAL
    sock = socket(AF_INET, SOCK_DGRAM, 0);
    if (getsockname(sock, &addr.sa, &size)) {
#else
    if (getpeername(sock, &addr.sa, &size)) {
#endif
	if (errno == ENOTSOCK)
	    return 1;
	else
	    return -1;
    }

#ifdef TEST_LOCAL
    memcpy(&addr, &tstaddr, sizeof(addr));
#endif

    type = addr.ss.ss_family;
    D(printf("address type of peer socket: %d\n", type));

    switch(type) {
	case AF_INET:
	    break;
#ifdef HAVE_IPV6
	case AF_INET6:
	    break;
#endif
	default:
	    D(printf("address type not supported.\n"));
	    return -2;
    }

    D(pat(&addr.sa));

#ifdef HAVE_IPV6
    if (type == AF_INET6) {
	struct sockaddr_in6 *i6 = &addr.sin6;
	D((printf("IPv6 address\n")));
	/* IPv6 localhost */
	if (IN6_IS_ADDR_LOOPBACK(&i6->sin6_addr)) {
	    D((printf("IPv6 loopback address\n")));
	    return 1;
	} else if (IN6_IS_ADDR_LINKLOCAL(&i6->sin6_addr)) {
	    D((printf("IPv6 link local address\n")));
	    return 1;
	} else if (IN6_IS_ADDR_SITELOCAL(&i6->sin6_addr)) {
	    D((printf("IPv6 site local address\n")));
	    return 1;
	} else if (IN6_IS_ADDR_V4MAPPED(&i6->sin6_addr)) {
	    /* map to IPv4 */
	    struct sockaddr_in si;
	    D((printf("IPv4 mapped IPv6 address\n")));
	    extract_v6mappedv4(i6, &si);
	    memcpy(&addr.sin, &si, sizeof(struct sockaddr_in));
	} else {
	    return 0;
	}
    }
#endif

    D(pat(&addr.sa));

    buf = mastr_new(2048);

    newsock = socket(PF_INET, SOCK_DGRAM, 0);
    if (sock < 0)
	return -1;

    /* get list of address information */
    for (;;) {
	ifc.ifc_len = mastr_size(buf);
	ifc.ifc_buf = mastr_modifyable_str(buf);
	if (ioctl(newsock, SIOCGIFCONF, (char *)&ifc) < 0) {
	    if (errno != EINVAL) {
		close(sock);
		mastr_delete(buf);
		return -1;
	    }
	} 

	/* work around bugs in old Solaris (see Postfix'
	 * inet_addr_local.c for details) */
	if ((unsigned)ifc.ifc_len < mastr_size(buf) / 2) {
	    break;
	}

	mastr_resizekill(buf, mastr_size(buf) * 2);
    }

    /* get addresses and netmasks */
    end = (struct ifreq *)((char *)ifc.ifc_buf + ifc.ifc_len);
    for (ifr = ifc.ifc_req ; ifr < end ;
#ifdef HAVE_SALEN
	    ifr = (ifr->ifr_addr.sa_len > sizeof(ifr->ifr_ifru)) ?
	    	((struct ifreq *)((char *) ifr
			     	+ offsetof(struct ifreq, ifr_ifru)
			     	+ ifr->ifr_addr.sa_len)) : ifr + 1
#else
	    ifr++
#endif
	    )
    {
	    struct in_addr sia;
	    
#ifdef HAVE_SALEN
	    D(printf("interface: name %s, address type: %d, sa_len: %d\n", ifr->ifr_name,
			ifr->ifr_addr.sa_family, ifr->ifr_addr.sa_len));
#else
	    D(printf("interface: name %s, address type: %d\n", ifr->ifr_name,
			ifr->ifr_addr.sa_family));
#endif
	    sia = ((struct sockaddr_in *)&ifr->ifr_addr)->sin_addr;
	    switch (ifr->ifr_addr.sa_family) {
		case AF_INET:
		    break;
#ifdef HAVE_IPV6
		case AF_INET6:
		    break;
#endif
		default:
		    continue;
	    }
	    D(pat((struct sockaddr *)&ifr->ifr_addr));
	    if (sia.s_addr != INADDR_ANY) {
		struct in_addr adr, msk;
#ifdef HAVE_SIOCGIFALIAS
		struct in_aliasreq in_ar;
#else
		struct ifreq ir;
#endif

#ifdef HAVE_SIOCGIFALIAS
		strlcpy(in_ar.ifra_name, ifr->ifr_name, sizeof(in_ar.ifra_name));
		memcpy(&in_ar.ifra_addr, &ifr->ifr_addr, sizeof(ifr->ifr_addr));
		if (ioctl(newsock, SIOCGIFALIAS, &in_ar) < 0)
		    goto bail_errno;
		adr = in_ar.ifra_addr.sin_addr;
		msk = in_ar.ifra_mask.sin_addr;
#else
		memcpy(&ir, ifr, sizeof(struct ifreq));
		/* Prevent nasty surprises on old Linux kernels with
		 * BSD-style IP aliasing (more than one IPv4 address on
		 * the same interface) -- SIOCGIFADDR/...NETMASK will
		 * return address and netmask for the first address,
		 * while SIOCGIFCONF will return the alias address, so
		 * there's no way to figure the NETMASK for these
		 * addresses. */
		if (ioctl(newsock, SIOCGIFADDR, &ir) < 0)
		    goto bail_errno;
		adr = ((struct sockaddr_in *)(&ir.ifr_addr))->sin_addr;
		if (adr.s_addr != sia.s_addr) {
		    char *buf;
		    buf = critstrdup(inet_ntoa(sia), "checkpeerlocal");
		    syslog(LOG_CRIT, "Problem: your kernel cannot deal with 4.4BSD-style IP aliases properly. "
			    "SIOCGIFADDR for interface %s address %s yields %s -> mismatch. "
			    "Configure Solaris-style aliases like %s:0 instead. Ignoring this interface, addresses in its range will be considered remote.",
			    ifr->ifr_name, buf, inet_ntoa(adr),
			    ifr->ifr_name);
		    D(printf("Kernel does not handle 4.4BSD-style IP alias for interface %s address %s, ignoring this interface.\n",
				ifr->ifr_name, buf));
		    free(buf);
		    continue;
		}
		memcpy(&ir, ifr, sizeof(struct ifreq));
		if (ioctl(newsock, SIOCGIFNETMASK, &ir) < 0)
		    goto bail_errno;
		msk = ((struct sockaddr_in *)(&ir.ifr_addr))->sin_addr;
#endif

		D(printf("address/netmask: %s/", inet_ntoa(adr)));
		D(printf("%s\n", inet_ntoa(msk)));

		if ((addr.sin.sin_addr.s_addr & msk.s_addr)
			== (adr.s_addr & msk.s_addr)) {
		    D(printf("found\n"));
		    close(newsock);
		    mastr_delete(buf);
		    return 1;
		}
	    }
    }

    close(newsock);
    mastr_delete(buf);
    return 0;

bail_errno:
    close(newsock);
    mastr_delete(buf);
    return -1;
}
#endif

#ifdef TEST
#include <string.h>

int verbose = 0;
int debug = 0;

int main(int argc, char *argv[])
{
#ifdef HAVE_IPV6
    int i;
    for (i = 1; i < argc; i++) {
	struct addrinfo hints, *res, *aii;
	int x;

	if (i > 1)
	    printf("\n");
	printf("=== argument %s ===\n", argv[i]);
	memset(&hints, 0, sizeof(hints));
	hints.ai_family = AF_UNSPEC;
	hints.ai_socktype = SOCK_STREAM;
	x = getaddrinfo(argv[i], NULL, &hints, &res);
	if (x) {
	    fprintf(stderr, "getaddrinfo \"%s\" failure: %s\n", argv[i], gai_strerror(x));
	    continue;
	}

	for (aii = res; aii != NULL; aii=aii->ai_next) {
	    printf("--> at position %p got address ", (void *)aii);
	    pat(aii->ai_addr);
	    memcpy(&tstaddr, aii->ai_addr, aii->ai_addrlen);
	    x = checkpeerlocal(0);
	    printf("checkpeerlocal(\"%s\") returns %d%s%s\n", argv[i], x, x == -1 ? " " : "", x == -1 ? gai_strerror(x) : "");
	}

	freeaddrinfo(res);
    }
#else
    int r;
    (void)argc; (void)argv;
    printf("checkpeerlocal returned: %d\n", (r = checkpeerlocal(0)));
    if (r == -1) printf("errno: %d (%s)\n", errno, strerror(errno));
#endif
    return 0;
}
#endif
