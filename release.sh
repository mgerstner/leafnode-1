#! /bin/sh

set -e
build=autobuild

###
### sanity and up-to-date checks
###
if ! grep -q $(TZ=UTC date +%Y-%02m-%02d) ChangeLog ; then
    echo "ChangeLog does not contain an entry for today" >&2
    exit 1
fi
if test "$IGNORE_UNCOMMITTED_GIT" = "" \
&& test "$(git status --porcelain | grep -v '^??')" != "" ; then
    echo "commit your changes to GIT first!"
    exit 1
fi
echo "Did you update NEWS? If not, please abort now."
sleep 5

dest=~/public_html/leafnode/
vers=$(perl -n -l -e 'if (/AC_INIT\(.+,\[(.+)\]\)/) { print "$1\n"; last; }' configure.ac)

### set QUICK=quick to skip the release stage and do just the upload
if [ x$QUICK != xquick ] ;then
    mkdir -p $build
    ( cd $build && ../configure -C --enable-maintainer-mode )
    if echo "$vers" | grep -E "devel|rc" ; then
	UPLOAD=no
	TARGZ=no
    else
	UPLOAD=yes
	TARGZ=yes
    fi

    echo "release for leafnode $vers"
    if test -f $dest/leafnode-$vers.tar.bz2 ; then
	echo "version $vers has already been released" 1>&2
	if [ "x$FORCE" != xforce ] ; then exit 1 ; fi
    fi
    make -j4 -C $build check
    make -j4 -C $build distcheck
    bunzip2 -c $build/leafnode-$vers.tar.bz2 >$dest/leafnode-$vers.tar
    rm -f $dest/leafnode-$vers.tar.xz
    xz --best $dest/leafnode-$vers.tar
    if [ ! -e $dest/leafnode-$vers.tar.bz2 ] ; then
	cp -p $build/leafnode-$vers.tar.bz2 $dest/
	if test $TARGZ = yes ; then
	    cp -p $build/leafnode-$vers.tar.gz $dest/
	fi
    fi
    cp -p NEWS $dest/
    cp -p README $dest/leafnode-readme.txt
    cp -p FAQ.xml $dest/
    for sufx in html txt pdf ; do cp -p $build/FAQ.$sufx $dest/ ; done
    cp -p ChangeLog $dest/ChangeLog.txt
    (
	cd $dest
	if test ! -e leafnode-$vers.tar.xz.asc -o ! leafnode-$vers.tar.xz.asc -nt leafnode-$vers.tar.xz ; then
	    gpg -ba --sign leafnode-$vers.tar.xz
	fi
	if test ! -e leafnode-$vers.tar.bz2.asc -o ! leafnode-$vers.tar.bz2.asc -nt leafnode-$vers.tar.bz2 ; then
	    gpg -ba --sign leafnode-$vers.tar.bz2
	fi
	if test $TARGZ = yes && test ! -e leafnode-$vers.tar.gz.asc -o !  leafnode-$vers.tar.gz.asc -nt leafnode-$vers.tar.gz ; then
	    gpg -ba --sign leafnode-$vers.tar.gz
	fi
    )
    vim $dest/RELEASE
    vim $dest/HEADER.html
    if test $TARGZ = yes ; then
	bash -x ./addfiles.sh
    fi
else
    UPLOAD=yes
fi

### Upload here
./export.sh
~/synchome.sh
if [ $UPLOAD = yes ] ; then
    printf 'lcd public_html/leafnode\ncd /home/frs/project/l/le/leafnode/leafnode/%s\nput leafnode-%s.tar.xz\nput leafnode-%s.tar.xz.asc\nput leafnode-%s.tar.bz2\nput leafnode-%s.tar.bz2.asc\nquit\n' $vers $vers $vers $vers $vers \
	| ssh krusty sftp -b - m-a@frs.sourceforge.net
fi
