## Process this file with automake to produce Makefile.in
#
# (C) 2001 - 2006, 2008, 2009, 2012 Matthias Andree
#
# This file is part of leafnode.
#
# This library is free software; you can redistribute it and/or modify
# it under the terms of the GNU Lesser General Public License as
# published by the Free Software Foundation; either version 2 or 2.1 of
# the License. See the file COPYING.LGPL for details.
#
# This library is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public
# License along with this library; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
# USA

SUBDIRS=. doc_german
AM_MAKEFLAGS='LDFLAGS=$(LDFLAGS)'
AUTOMAKE_OPTIONS=foreign dist-bzip2 no-installinfo 1.5

CC = @CC@ $(GCC_FLAGS)

# additional autoconf stuff
RPM = @RPM@

BUILT_SOURCES=config.c

# globals
sbin_PROGRAMS=leafnode fetchnews texpire checkgroups applyfilter
bin_PROGRAMS=newsq leafnode-version
noinst_PROGRAMS=quickmkdir amiroot lsort
# quickmkdir is no longer referenced by this Makefile,
# as it is no longer necessary. It is kept available for compatibility,
# so that existing scripts to wrap leafnode in a package continue to
# work
noinst_SCRIPTS=update.sh makesubst subst setup-daemontools.sh \
	       leafnode.cron.daily

noinst_DATA=leafnode.xinetd

sysconf_DATA=config.example filters.example run.tcpserver.dist run.tcpd.dist \
    nntp.rules.dist Makefile.dist UNINSTALL-daemontools
mydocs=README-FQDN README-FQDN.html README-FQDN.pdf \
       README-daemontools \
       FAQ.html FAQ.txt FAQ.pdf

man_MANS=applyfilter.8 checkgroups.8 fetchnews.8 leafnode.8\
	newsq.1 leafnode-version.1 texpire.8
EXTRA_DIST=leafnode.spec leafnode.spec.in $(sysconf_DATA) CREDITS \
	ChangeLog ChangeLog.old sanitize.sed \
	README-MAINTAINER update.sh COPYING.LGPL COPYING.GPL \
	getline.c mkstemp.c strdup.c strlcpy.c strlcpy.h \
	.indent.pro config.h.in cindent README-FQDN.pod \
	$(mydocs) $(MANPAGES_IN) makesubst subst.in \
	FAQ.xml timegm.c \
	run.tcpserver.dist.in run.tcpd.dist.in setup-daemontools.sh.in \
	UNINSTALL-daemontools leafnode.cron.daily.in \
	t.pcre_extract .lclintrc KNOWNBUGS \
	leafnode-SA-2002-01.txt \
	leafnode-SA-2003-01.txt \
	leafnode-SA-2004-01.txt \
	leafnode-SA-2005-01.txt \
	leafnode-SA-2005-02.txt \
	t/cmp.gal1t t/cmp.gal1w \
	t/cmp.gal2t t/cmp.gal2w \
	t/cmp.gal3t t/cmp.gal3w \
	t/cmp.gal4t t/cmp.gal4w \
	t/cmp.gal5t t/cmp.gal5w \
	t/cmp.gal6t t/cmp.gal6w \
	ADD-ONS leafnode.xinetd.in OLDNEWS tools/newsq.pl tools/archivefaq.pl \
	GMakefile.doc preamble.tex

MAINTAINERCLEANFILES=$(mydocs)

README-FQDN.html: README-FQDN.pod
	pod2html >$@ $(top_srcdir)/README-FQDN.pod \
	|| { rm -f $@ ; false ; }
	rm -f *~~

README-FQDN: README-FQDN.pod
	pod2text >$@ $(top_srcdir)/README-FQDN.pod \
	|| { rm -f $@ ; false ; }

README-FQDN.tex: README-FQDN.pod preamble.tex
	pod2latex -full -prefile $(top_srcdir)/preamble.tex $(top_srcdir)/README-FQDN.pod

README-FQDN.pdf: README-FQDN.tex
	pdflatex README-FQDN.tex
	pdflatex README-FQDN.tex
	texindex README-FQDN.idx
	pdflatex README-FQDN.tex

LDADD=@LIBOBJS@ liblnutil.a @PCRELIB@

# particulars
leafnode_SOURCES=nntpd.c nntpd_dodate.c nntpd.h leafnode.h

fetchnews_SOURCES=fetchnews.c fetchnews_check_date.c fetchnews.h

filterutil.o: filterutil.c $(PCREDEP)

MANPAGES_IN=applyfilter.8.in checkgroups.8.in fetchnews.8.in \
	leafnode.8.in texpire.8.in leafnode-version.1.in newsq.1.in
applyfilter.8: $(srcdir)/applyfilter.8.in subst
	$(SHELL) ./subst <$(srcdir)/applyfilter.8.in >$@
checkgroups.8: $(srcdir)/checkgroups.8.in subst
	$(SHELL) ./subst <$(srcdir)/checkgroups.8.in >$@
fetchnews.8: $(srcdir)/fetchnews.8.in subst
	$(SHELL) ./subst <$(srcdir)/fetchnews.8.in >$@
leafnode.8: $(srcdir)/leafnode.8.in subst
	$(SHELL) ./subst <$(srcdir)/leafnode.8.in >$@
texpire.8: $(srcdir)/texpire.8.in subst
	$(SHELL) ./subst <$(srcdir)/texpire.8.in >$@
setup-daemontools.sh: $(srcdir)/setup-daemontools.sh.in subst
	$(SHELL) ./subst <$(srcdir)/setup-daemontools.sh.in >$@
leafnode-version.1: $(srcdir)/leafnode-version.1.in subst
	$(SHELL) ./subst <$(srcdir)/leafnode-version.1.in >$@
newsq.1: $(srcdir)/newsq.1.in subst
	$(SHELL) ./subst <$(srcdir)/newsq.1.in >$@

run.tcpserver.dist: $(srcdir)/run.tcpserver.dist.in subst
	$(SHELL) ./subst <$(srcdir)/run.tcpserver.dist.in >$@
run.tcpd.dist: $(srcdir)/run.tcpd.dist.in subst
	$(SHELL) ./subst <$(srcdir)/run.tcpd.dist.in >$@

leafnode.cron.daily: $(srcdir)/leafnode.cron.daily.in subst
	$(SHELL) ./subst <$(srcdir)/leafnode.cron.daily.in >$@

$(srcdir)/subst.in: $(srcdir)/makesubst $(srcdir)/configure.ac
	$(SHELL) $(srcdir)/makesubst $(srcdir)/configure.ac >$@

leafnode.xinetd: leafnode.xinetd.in Makefile
	echo '# THIS IS A GENERATED FILE - MAINTAINER, EDIT THE .in FILE INSTEAD' >$@ \
	&& sed -e 's}[@]sbindir@}$(sbindir)}g' \
	       -e 's}[@]NEWS_USER@}$(NEWS_USER)}g' \
	    <$(srcdir)/leafnode.xinetd.in >>$@ || rm -f $@

.PHONY: rpm
rpm:	leafnode.spec
	if test @RPMSRC@ = none ; then \
	  echo "No RPM build dir. RPM not installed?" 1>&2 ; exit 2 ; fi
	$(MAKE) dist-bzip2
	rm -f @RPMSRC@/SOURCES/@PACKAGE@-@VERSION@.tar.bz2
	cp @PACKAGE@-@VERSION@.tar.bz2 @RPMSRC@/SOURCES
	rpmbuild -ba leafnode.spec || rpm -ba leafnode.spec

install-data-hook: amiroot
	set -e ; for i in "" /leaf.node /failed.postings /interesting.groups \
		/out.going /message.id /temp.files ; do \
        mkdir -p $(DESTDIR)$(SPOOLDIR)$$i ; \
	if ./amiroot ; then \
	 chown @NEWS_USER@:@NEWS_GROUP@ $(DESTDIR)$(SPOOLDIR)/$$i ; \
	 chmod 2775      $(DESTDIR)$(SPOOLDIR)/$$i ; fi ; \
	done
	d="`dirname $(DESTDIR)@LOCKFILE@`" ; mkdir -p "$${d}" && \
	if ./amiroot ; then \
          chown @NEWS_USER@:@NEWS_GROUP@ "$${d}" ; \
	  chmod 2775 "$${d}" ; fi

uninstall-hook:
	rm -f $(DESTDIR)@LOCKFILE@
	@echo
	@echo '===================================================='
	@echo 'If you want to delete leafnode'"'"'s news spool, type:'
	@echo 'rm -rf "$(DESTDIR)$(SPOOLDIR)"'
	@echo
	@echo 'Note that this will permanently remove articles from'
	@echo 'all newsgroups, including local and archived groups!'
	@echo '===================================================='
	@echo

.PHONY: update
update: lsort
	$(SHELL) $(srcdir)/update.sh "$(SPOOLDIR)" "$(sysconfdir)" "$(LOCKFILE)" "@NEWS_USER@" "@NEWS_GROUP@"

config.c: Makefile configure
	( echo '/* DO NOT EDIT! This file is auto-generated by Makefile. */' ; \
	  echo '/* user programs go into @bindir@ */'; \
	  echo '/* system programs go into @sbindir@ */'; \
	  echo '#include "leafnode.h"'; \
	  echo 'const char *spooldir = "@SPOOLDIR@" ;' ; \
	  echo 'const char *sysconfdir = "@sysconfdir@" ;' ; \
	  echo 'const char *lockfile = "@LOCKFILE@" ;' ; \
	  echo 'const char *version  = "@VERSION@" ;' ) >$@ \
	|| rm -f $@

noinst_LIBRARIES=liblnutil.a
liblnutil_a_SOURCES=nntputil.c configutil.c xoverutil.c activutil.c \
	activutil_resolve.c activutil.h agetcwd.c \
	miscutil.c artutil.c filterutil.c syslog.c getaline.c \
	critmem.c critmem.h lockfile.c validatefqdn.c mgetaline.c \
	leafnode.h mastring.h mastring.c getline.h \
	wildmat.c validatefqdn.h getfoldedline.c writes.c ln_log.c \
	ln_log.h grouplist.c checkpeerlocal.c pcre_extract.c version.h \
	masock_sa2name.c masock.h groupselect.c groupselect.h system.h \
	mysigact.h mysigact.c log_unlink.c wantassert.h snprintf.c

nodist_liblnutil_a_SOURCES=config.c

# for testing
check_PROGRAMS = test1 testgen xsnprintf checkpeerlocal validatefqdn \
		 pcre_extract nntputil t.rfctime grouplist
dist_check_SCRIPTS = t.validate-xml
checkpeerlocal_SOURCES	= checkpeerlocal.c
t_rfctime_SOURCES = t.rfctime.c
checkpeerlocal_CFLAGS	= -DTEST -DTEST_LOCAL $(AM_CFLAGS) $(CFLAGS)
pcre_extract_CFLAGS = -DTEST $(AM_CFLAGS) $(CFLAGS)
nntputil_CFLAGS=-DMAIN $(AM_CFLAGS) $(CFLAGS)
validatefqdn_CFLAGS=-DTEST  $(AM_CFLAGS) $(CFLAGS)
grouplist_CFLAGS=-DTEST  $(AM_CFLAGS) $(CFLAGS)

MYTESTS=\
t.gal2 t.gal3 t.gal4 t.gal6
$(MYTESTS): Makefile \
  $(top_srcdir)/t/cmp.gal1t $(top_srcdir)/t/cmp.gal1w \
  $(top_srcdir)/t/cmp.gal2t $(top_srcdir)/t/cmp.gal2w \
  $(top_srcdir)/t/cmp.gal3t $(top_srcdir)/t/cmp.gal3w \
  $(top_srcdir)/t/cmp.gal4t $(top_srcdir)/t/cmp.gal4w \
  $(top_srcdir)/t/cmp.gal5t $(top_srcdir)/t/cmp.gal5w \
  $(top_srcdir)/t/cmp.gal6t $(top_srcdir)/t/cmp.gal6w
$(MYTESTS):
	i() { ( echo '#! /bin/sh' ; \
	echo "./test1 <\"$$1\" | cmp - \"$$2\"" ); }; i \
		`echo $(top_srcdir)/$@ | sed 's/t\.\(.*\)/t\/cmp.\1t/'` \
		`echo $(top_srcdir)/$@ | sed 's/t\.\(.*\)/t\/cmp.\1w/'` \
		>$@ && chmod +x $@

TESTS=$(MYTESTS) xsnprintf t.pcre_extract t.validate-xml
TESTS_ENVIRONMENT=srcdir=$(srcdir) SHELL="$(SHELL)"

CLEANFILES=$(MYTESTS) xsnprintf $(man_MANS) FAQ.aux FAQ.tex FAQ.out FAQ.log \
	   README-FQDN.aux README-FQDN.log README-FQDN.ind README-FQDN.idx \
	   README-FQDN.toc README-FQDN.pdf README-FQDN.idxs README-FQDN.tex

distclean-local:
	rm -f README-FQDN README-FQDN.html pod2html-itemcache pod2html-dircache
	rm -f config.c stamp-h leafnode.cron.daily leafnode.xinetd
	rm -f setup-daemontools.sh run.tcpserver.dist run.tcpd.dist
	rm -f FAQ.html FAQ.txt FAQ.pdf FAQ.fo

# NOTE: $< does not work with BSD make
# ####################################
#FAQ.tex: FAQ.xml
#	openjade -t tex -V %generate-article-toc% -V '%paper-type%=A4' -d /usr/share/sgml/docbook/dsssl-stylesheets-1.79/print/docbook.dsl -o $@ $< \
#	|| { rm -f $@ ; false ; }
#
#FAQ.pdf: FAQ.tex
#	pdfjadetex $< || { rm -f $@ ; false ; }
#	pdfjadetex $< || { rm -f $@ ; false ; }
#	pdfjadetex $< || { rm -f $@ ; false ; }

FAQ.fo: FAQ.xml
	xmlto fo --extensions $(srcdir)/FAQ.xml

FAQ.pdf: FAQ.fo
#	$$HOME/XEP/xep -quiet -xml FAQ.fo -pdf $@
	fop FAQ.fo $@

FAQ.html: FAQ.xml
	xmlto html-nochunks --extensions $(srcdir)/FAQ.xml

FAQ.txt: FAQ.html
	w3m -dump FAQ.html >$@

docs:
	gmake -f $(top_srcdir)/GMakefile.doc
	cd doc_german && $(MAKE) docs

# this target expects a .rsyncs file with lines of this format:
# host:directory/
# it will call rsync from its source directory to the destination for
# each of them, running them in parallel
.PHONY: rsync rsynconly
rsync:	distdir rsynconly
rsynconly:	$(srcdir)/.rsyncs
	@( cat $(srcdir)/.rsyncs | sed -e 's}^}rsync -avessh --delete $(PACKAGE)-$(VERSION)/ }; s/\($$\)/ \&/;' ; echo "wait" ) | $(SHELL) -x
