#! /bin/sh
set -ex
cd "$(dirname "$0")"
rsync -Hrlt -vP -e ssh \
* \
m-a,leafnode@web.sourceforge.net:/home/groups/l/le/leafnode/htdocs/ \
--delete-excluded --delete --exclude "*~" \
"$@" &
rsync -Hrlt -vP * ~/public_html/leafnode/nw/ \
--delete-excluded --delete --exclude "*~" \
"$@" &
wait
