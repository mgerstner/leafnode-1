#include "leafnode.h"
#include <unistd.h>

/** allocates a buffer (stores result into *buf and *size)
 * and fetches the current working directory.
 * *buf must be a malloc()d buffer or NULL on first call.
 * returns success, *buf is free()d when FALSE is returned */
int agetcwd(char **buf, size_t *size) {
    while(1) {
	if (*buf) {
	    if (getcwd(*buf, *size - 1))
		return TRUE;
	    free(*buf);
	    *buf = NULL;
	    if (errno != ERANGE) {
		return FALSE;
	    }
	    *size <<= 1;
	} else {
	    *size = PATH_MAX;
	}
	*buf = critmalloc(*size, "agetcwd");
	(*buf)[*size-1] = 0; /* NUL terminate always */
    }
}


