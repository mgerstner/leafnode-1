#include "leafnode.h"

#include "config.h"
#include <signal.h>
#include "mysigact.h"

int mysigact(int sig, int flags, void (* func)(int), int blockthis)
{
    struct sigaction sa;

    sa.sa_handler = func;
    sa.sa_flags = SA_NOCLDSTOP | flags;
    sigemptyset(&sa.sa_mask);
    if (blockthis)
	sigaddset(&sa.sa_mask, blockthis);
    return sigaction(sig, &sa, (struct sigaction *)0);
}
