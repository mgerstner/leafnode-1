#include "leafnode.h"

#include <string.h>
#include <unistd.h>

ssize_t
writes(int fd, const char *string)
{
    return write(fd, string, strlen(string));
}
